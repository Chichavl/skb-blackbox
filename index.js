/**
 * Created by Vadim on 18.11.2016.
 */
var express = require('express')
var app = express()

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    var arr = [];
    arr[0] = 1;
    arr[1] = 18;
    arr[2] = 243;
    arr[3] = 3240;
    arr[4] = 43254;
    arr[5] = 577368;
    arr[6] = 7706988;
    arr[7] = 102876480;
    arr[8] = 1373243544;
    arr[9] = 18330699168;
    arr[10] = 244686773808;
    arr[11] = 3266193870720;
    arr[12] = 43598688377184;
    arr[13] = 581975750199168;
    arr[14] = 7768485393179328;
    arr[15] = 103697388221736960;
    arr[16] = 1384201395738071424;
    arr[17] = 18476969736848122368;
    arr[18] = 246639261965462754048;
    var input = parseInt(req.query.i);
    var output = arr[input];

    res.send(output.toString());

});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});